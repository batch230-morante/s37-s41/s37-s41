const mongoose = require("mongoose");
const Course = require("../models/course");


// function for adding course
// 2. Update the "addCourse" controller method to implement admin authentication for creating a course
// {NOTE} incluse a sc of successful admin addcourse and sc of not successful


/*
module.exports.addCourse = (reqBody, newData) => {
    let newCourse = new Course({
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    })
*/

module.exports.addCourse = (reqBody, newData) => {
    if(newData.isAdmin == true){
        let newCourse = new Course({
            name: reqBody.name,
            description: reqBody.description,
            price: reqBody.price,
            slots: reqBody.slots
        })
        return newCourse.save().then((newCourse, error) => {
            if(error){
                return error;
            }
            else{
                return newCourse;
            }
        })
    }
    else{
        let message = Promise.resolve("User must be ADMIN to access this functionality");
        return message.then((value) => {return value});
    }
}


// GET all courses
module.exports.getAllCourses = () => {
    return Course.find({}).then(result => {
        return result;
    })
}


// GET all active courses
module.exports.getActiveCourses = () => {
    return Course.find({isActive: true}).then(result => {
        return result;
    })
}


// GET specific course
module.exports.getCourse = (courseId) => {
    return Course.findById(courseId).then(result => {
        return result;
    })
}


// UPDATING a course
module.exports.updateCourse = (courseId, newData) => {
    if(newData.isAdmin == true){
    return Course.findByIdAndUpdate(courseId,
        {
            // newData.course.name
            // newData.course.request.body
        name: newData.course.name,
        description: newData.course.description,
        price: newData.course.price
        }
        ).then((result, error) => {
            if(error){
                return false;
            }
            else{
                return result;
            }
        })
    }
    else{
        // promise.resolve() is required if it expects a solved promise
        let message = Promise.resolve("User must be ADMIN to access this functionality");
        return message.then((value) => {return value});
    }
}

module.exports.archiveCourse = (courseId, newData) => {
    if(newData.isAdmin == true){
    return Course.findByIdAndUpdate(courseId,
        {
        isActive: newData.course.isActive,
        }
        ).then((result, error) => {
            if(error){
                return false;
            }
            else{
                return true;
            }
        })
    }
    else{
        let message = Promise.resolve("User must be ADMIN to access this functionality");
        return message.then((value) => {return value});
    }
}

