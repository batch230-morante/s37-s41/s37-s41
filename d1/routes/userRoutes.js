const { response } = require("express");
const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers");
const auth = require("../auth");


router.post("/register", (request, response) => {
	userControllers.registerUser(request.body).then(resultFromController => response.send(resultFromController));
})

router.post("/checkEmail", (request, response) => {
    userControllers.checkEmailExist(request.body).then(resultFromController => response.send(resultFromController));
})


router.post("/login", (request, response) => {
    userControllers.loginUser(request.body).then(resultFromController => response.send(resultFromController));
})



router.post("/details", (request, response) => {
    userControllers.getProfile(request.body).then(resultFromController => response.send(resultFromController));
})



// router.get("/detailsv2", auth.verify, userControllers.getProfileB);



router.post("/enroll", auth.verify, userControllers.enroll);

// ------------------------------------
router.get("/allUsers", userControllers.getAllUsersController);



module.exports = router;
