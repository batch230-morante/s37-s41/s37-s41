const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseControllers");
const auth = require("../auth");
const { response } = require("express");

// Activity
// 1. Refractor the "course" route to implement user authentication for the admin when creating a course

router.post("/create", auth.verify, (request, response) => {
	const newData = {
		course: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}
    courseController.addCourse(request.body, newData).then(resultFromController => response.send(resultFromController))
});

/*
router.patch("/:courseId/update", auth.verify, (request,response) => 
{
	const newData = {
		course: request.body, 	//request.headers.authorization contains jwt
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	courseController.updateCourse(request.params.courseId, newData).then(resultFromController => {
		response.send(resultFromController)
	})
})

*/

// Router for getting all courses (GET all courses)
router.get("/all", (request, response) => {
    courseController.getAllCourses().then(resultFromController => response.send(resultFromController))
})


// GET active courses
router.get("/activeCourses", (request, response) => {
    courseController.getActiveCourses().then(resultFromController => response.send(resultFromController))
})

// GET specific course
router.get("/:courseId", (request, response) => {
    courseController.getCourse(request.params.courseId).then(resultFromController => response.send(resultFromController))
})



// PATCH course (update)
// router.patch("/:courseId/update", (request, response) => {
//     const newData = {
//         course: request.body,
//         isAdmin: auth.decode(request.headers.authorization).isAdmin
//     }
//     courseController.updateCourse(request.params.courseId).then(resultFromController => response.send(resultFromController))
// })

router.patch("/:courseId/update", auth.verify, (request,response) => 
{
	const newData = {
		course: request.body, 	//request.headers.authorization contains jwt
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	courseController.updateCourse(request.params.courseId, newData).then(resultFromController => {
		response.send(resultFromController)
	})
})


router.patch("/:courseId/archive", auth.verify, (request,response) => 
{
	const newData = {
		course: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	courseController.archiveCourse(request.params.courseId, newData).then(resultFromController => {
		response.send(resultFromController)
	})
})

module.exports = router;